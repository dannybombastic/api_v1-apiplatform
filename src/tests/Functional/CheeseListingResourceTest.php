<?php


declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\CheeseListing;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class CheeseListingResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;
    public function testCreateCheeseListing()
    {
        $client = self::createClient();
        $client->request('POST', '/api/cheeses', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => []
        ]);

        $this->assertResponseStatusCodeSame(401);

        $this->createUserLogin($client, "pan@pan.com", "659011563");

        $client->request('POST', '/api/cheeses', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => []
        ]);

        $this->assertResponseStatusCodeSame(400);
    }

    public function testUpdateCheeseListing()
    {
        $client = self::createClient();
        $email = "pan@pan.com";
        $password = "659011563";

        $email_user_two = "panochas@pan.com";
        $password_user_two = "606197854";

        $user_two =  $this->createUser($email_user_two, $password_user_two);

        $user =  $this->createUser($email, $password);

        $cheeseListing = new CheeseListing();
        $cheeseListing->setTitle("block of chedar");
        $cheeseListing->setTextDescription("amazing cheese from goat");
        $cheeseListing->setOwner($user_two);
        $cheeseListing->setPrice(500);

        $em = $this->getEntityManager();
        $em->persist($cheeseListing);
        $em->flush();

        $this->logIn($client, $email, $password);

        $client->request('PUT', '/api/cheeses/' . $cheeseListing->getId(), [
            'json' => ['title' => 'update cheese', 'owner' => '/api/users/'.$user->getId()]
        ]);

        $this->assertResponseStatusCodeSame(403);
    }
}
