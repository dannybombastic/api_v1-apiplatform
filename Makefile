#!/bin/bash


 

OS := $(shell uname)
DOCKER_BE = api_v1_php-fpm-api_1

UID = $(shell id -u)
ROOTUID = 0


help: ## Show this help message
	@echo 'usage: make [target]' 
	@echo
	@echo 'targets:'
	@egrep '^(.+)\:\ ##\ (.+)' ${MAKEFILE_LIST} | column -t -c 2 -s ':#'

run: ## Start the containers

	U_ID=${UID} docker-compose up 


stop: ## Stop the containers

	U_ID=${UID} docker-compose stop


docker-sync-restart: ## Rebuild docker-sync stack and prepare environment
	U_ID=${UID} docker-sync-stack clean
	$(MAKE) run
	$(MAKE) prepare

restart: ## Restart the containers
	$(MAKE) stop && $(MAKE) run

build: ## Rebuilds all the containers
 
	U_ID=${UID} docker-compose build
 

prepare: ## Runs backend commands
	$(MAKE) be-sf-permissions
	$(MAKE) composer-install
	$(MAKE) migrations

# Backend commands
be-sf-permissions: ## Configure the Symfony permissions
	U_ID=${UID} docker exec -it -uroot ${DOCKER_BE} sh /usr/bin/sf-permissions

composer-install: ## Installs composer dependencies
	U_ID=${UID} docker exec --user ${UID} -it ${DOCKER_BE} composer install --no-scripts --no-interaction --optimize-autoloader

updatedb-test:
	U_ID=${ROOTUID} docker exec -w /var/www  -it --user  ${UID} ${DOCKER_BE}  bin/console doctrine:schema:update --force --env=test

mk: ## Runs the migrations
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} bin/console make:migration -n

migrations: ## Runs the migrations
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} bin/console doctrine:migrations:migrate -n

cache: ## Clear SF cache
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} bin/console cache:clear

be-logs: ## Tails the Symfony dev log
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} tail -f var/log/dev.log
# End backend commands

ssh-be: ## ssh's into the be container
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} bash

drop:
	U_ID=${ROOTUID} docker exec -it --user ${UID} ${DOCKER_BE}  bin/console doctrine:schema:drop --force --full-database 

test:
	U_ID=${ROOTUID} docker exec -w /var/www  -it --user  ${UID} ${DOCKER_BE}  bin/phpunit 

test-filter:
	U_ID=${ROOTUID} docker exec -w /var/www  -it --user  ${UID} ${DOCKER_BE}  bin/phpunit --filter=${FILTER}

createdb-test:
	U_ID=${ROOTUID} docker exec -it --user  ${UID} ${DOCKER_BE}  bin/console doctrine:schema:drop --force --full-database --env=test 
	U_ID=${ROOTUID} docker exec -it --user  ${UID} ${DOCKER_BE}  bin/console doctrine:database:drop --force --env=test 
	U_ID=${ROOTUID} docker exec -it --user  ${UID} ${DOCKER_BE}  bin/console doctrine:database:create --env=test
	U_ID=${ROOTUID} docker exec -it --user  ${UID} ${DOCKER_BE}  bin/console doctrine:schema:create --env=test

fixtures:
	U_ID=${ROOTUID} docker exec -it --user ${UID} ${DOCKER_BE}  bin/console doctrine:fixtures:load -n

code-style: ## Runs php-cs to fix code styling following Symfony rules
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE}  php-cs-fixer fix src/ --rules=@Symfony
#	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} php-cs-fixer fix tests --rules=@Symfony


generate-keys: ## Generate ssh keys in the container
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} mkdir -p config/jwt
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} openssl genrsa -passout pass:sf5-expenses-api -out config/jwt/private.pem -aes256 4096
	U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} openssl rsa -pubout -passin pass:sf5-expenses-api -in config/jwt/private.pem -out config/jwt/public.pem
