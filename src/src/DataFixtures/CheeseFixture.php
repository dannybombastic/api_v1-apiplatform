<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\CheeseListing;
use App\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CheeseFixture extends BaseFixture implements DependentFixtureInterface
{

    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(CheeseListing::class, 20, function (CheeseListing $cheese, $count) {

            $user = $this->getReference(User::class . '_' . $this->faker->numberBetween(0, 19));

            $cheese->setIsPublished($this->faker->boolean(40));
            $cheese->setTitle($this->faker->sentence($nbWords = 3, $variableNbWords = true));
            $cheese->setTextDescription($this->faker->realText($maxNbChars = 50, $indexSize = 2));
            $cheese->setPrice($this->faker->numberBetween(50, 400));
            $cheese->setOwner($user);
        });
        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            UserFixture::class,
        ];
    }
}
