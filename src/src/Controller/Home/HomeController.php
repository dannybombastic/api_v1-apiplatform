<?php

declare(strict_types=1);

namespace App\Controller\Home;

use ApiPlatform\Core\Api\IriConverterInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * @Route("/home")
 */

class HomeController extends AbstractController
{

    /**
     * @Route("/login", name="app_home")
     */
    public function home(SerializerInterface $serializer)
    {


        return $this->render('Home/home.html.twig',[
            'user' => $serializer->serialize($this->getUser(),'jsonld')
        ]);
    }
}
