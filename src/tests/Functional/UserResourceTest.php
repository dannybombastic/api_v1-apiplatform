<?php

declare(strict_types=1);


namespace App\Tests\Functional;

use App\Entity\User;
use App\Test\CustomApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;

class UserResourceTest extends CustomApiTestCase
{
    use ReloadDatabaseTrait;


    public function testCreateUser()
    {
        $client = self::createClient();

        $email = "dannybombastic@gmail.com";
        $password = "659011563";

        $client->request('POST', '/api/users', [
            'json' => [
                'email' => $email,
                'username' => 'dannybombastic',
                'password' => $password

            ],
        ]);

        $this->assertResponseStatusCodeSame(201);

        $this->logIn($client, $email, $password);
    }


    public function testUpdateUser()
    {
        $client = self::createClient();

        $user = $this->createUserLogin($client, "dan@dan.com", "659011563");

        $client->request('PUT', '/api/users/' . $user->getId(), [
            'json' => [
                'username' => 'newUserName',
                'roles' => ['ROLE_ADMIN']
            ]
        ]);

        $this->assertResponseIsSuccessful();
        $this->assertJsonContains([
            'username' => 'newUserName',
        ], true);

        $em = $this->getEntityManager();
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());

        $this->assertEquals(['ROLE_USER'], $user->getRoles());


    }


    public function testGetUser()
    {
        $client = self::createClient();
        $user = $this->createUserLogin($client, "dan@dan.com", "659011563");
        $user->setPhoneNumber('606197854');
        $em = $this->getEntityManager();
        $em->flush();

        $client->request('GET', '/api/users/'.$user->getId());
        $this->assertJsonContains([
            'username' => 'dan'
        ]);

        $data = $client->getResponse()->toArray();
        $this->assertArrayHasKey('phoneNumber', $data);
        
        // need to refresh the user because afther the request all instances were reset
        /** @var User $user */
        $user = $em->getRepository(User::class)->find($user->getId());
        $user->setRoles(['ROLE_ADMIN']);
        $em->flush();

        $this->logIn($client, "dan@dan.com", "659011563");

        $client->request('GET', '/api/users/'.$user->getId());
        
        $this->assertJsonContains([
            'phoneNumber' => '606197854'
        ]);


    }
}
