<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\BooleanFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use App\Repository\CheeseListingRepository;
use Carbon\Carbon;
use DateTime;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\SerializedName;
use Symfony\Component\Validator\Constraints as Assert;

// normalizationContext={"groups"="cheese_listing:read", "swagger_definition_name"="Read"},
// *    denormalizationContext={"groups"="cheese_listing:write", "swagger_definition_name"="Write"},

 
/**
 * @ApiResource(
 *    collectionOperations={
 *          "get",
 *          "post"={"security"="is_granted('ROLE_USER')"}
 *      },
 *    itemOperations={
 *          "get"={
 *             
 *          },
 *          "put"={
 *          "security"="is_granted('EDIT', object)",
 *           "security_message"="only the creator can edit cheese list"
 * 
 *          },
 *          "delete"={"security"="is_granted('ROLE_ADMIN')"},
 *          "patch"
 *     },
 *     
 *     
 *    shortName="cheese",
 *    attributes={
 *      "pagination_items_per_page"=10,
 *      "formats"={"jsonld", "json", "html", "jsonhal", "csv"="text/csv"}
 *      }
 * )
 * @ORM\Entity(repositoryClass=CheeseListingRepository::class)
 * @ApiFilter(BooleanFilter::class, properties={"isPublished"})
 * @ApiFilter(SearchFilter::class, properties={
 *      "title": "partial", 
 *      "description": "partial",
 *      "owner": "exact",
 *      "owner.username": "partial"
 *      
 *      })
 * @ApiFilter(RangeFilter::class, properties={"price"})
 * @ApiFilter(PropertyFilter::class)
 */
class CheeseListing
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"cheese:read", "cheese:write", "user:read", "user:write"})
     * @Assert\NotBlank()
     * @Assert\Length(min=2, max=50, maxMessage="Describe your cheese in 40 charaters")
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Groups({"cheese:read", "user:read"})
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * The price of this delicious cheese.
     *
     * @ORM\Column(type="integer")
     * @Groups({"cheese:read", "cheese:write", "user:read", "user:write"})
     * @Assert\NotBlank()
     */
    private $price;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createAt;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isPublished = false;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="cheeseListings")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"cheese:read", "cheese:write"})
     * @Assert\Valid()
     */
    private $owner;

    public function __construct()
    {
        $this->createAt = new DateTimeImmutable();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * raw text description from a cheese.
     *
     * @Groups({"cheese:read"})
     */
    public function getShortDescription(): ?string
    {
        if (strlen($this->description) < 40) {
            return $this->description;
        }

        return substr($this->description, 0, 40) . '...';
    }

    /**
     * raw text description from a cheese.
     *
     * @Groups({"cheese:write", "user:write"})
     * @SerializedName("description")
     */
    public function setTextDescription(string $description): self
    {
        $this->description = nl2br($description);

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCreateAt(): ?DateTime
    {
        return $this->createAt;
    }

    /**
     * how long ago in text that this cheese listing was added.
     *
     * @Groups({"cheese:read"})
     */
    public function getCreateAtAgo(): ?string
    {
        return  Carbon::instance($this->getCreateAt())->diffForHumans();
    }

    public function getIsPublished(): ?bool
    {
        return $this->isPublished;
    }

    public function setIsPublished(bool $isPublished): self
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Set the value of title.
     *
     * @return self
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
