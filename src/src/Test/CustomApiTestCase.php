<?php

declare(strict_types=1);


namespace App\Test;

use Doctrine\ORM\EntityManagerInterface;

use App\ApiPlatform\Test\ApiTestCase;
use App\ApiPlatform\Test\Client;
use App\Entity\User;

class CustomApiTestCase extends ApiTestCase
{


    protected function createUser(string $email, string $password): User
    {
        $user = new User();
        $user->setEmail($email);
        $user->setUsername(substr($email, 0, strpos($email, '@')));
        $encoder = self::$container->get('security.password_encoder')
            ->encodePassword(
                $user,
                $password
            );
        $user->setPassword($encoder);
        $user->setRoles(['ROLE_USER']);
        /**
         * @var EntityManagerInterface $em
         */
        $em = $this->getEntityManager();
        $em->persist($user);
        $em->flush();
        return $user;
    }


    protected function logIn(Client $client, string $email, string $password)
    {

        $client->request('POST', '/login', [
            'headers' => ['Content-Type' => 'application/json'],
            'json' => [
                'email' => $email,
                'password' => $password
            ]
        ]);
        $this->assertResponseStatusCodeSame(204);
    }


    protected function createUserLogin(Client $client, string $email, string $password): User
    {
        $user = $this->createUser($email, $password);

        $this->logIn($client, $email, $password);

        return $user;
    }


    protected function getEntityManager(): EntityManagerInterface
    {
        return self::$container->get('doctrine')->getManager();
    }
}
