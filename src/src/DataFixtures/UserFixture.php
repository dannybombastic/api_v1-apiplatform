<?php

namespace App\DataFixtures;

use App\DataFixtures\BaseFixture;
use App\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixture extends BaseFixture
{
    public function loadData(ObjectManager $manager)
    {
        $user_admin = new User();
        $user_admin->setUsername('dannybombastic');
        $user_admin->setEmail('dannybombastic@gmail.com');
        $user_admin->setRoles(['ROLE_ADMIN']);
        $user_admin->setPassword(
            $this->encoder->encodePassword(
                $user_admin,
                '659011563'
            )
        );
        $manager->persist($user_admin);
        $manager->flush();

        $this->createMany(User::class, 20, function (User $user, $count) {

            $user->setUsername($this->faker->name);
            $user->setEmail($this->faker->email);
            $user->setRoles(['ROLE_USER']);
            $user->setPassword(
                $this->encoder->encodePassword(
                    $user,
                    '659011563'
                )
            );

        });
        $manager->flush();
    }
}
